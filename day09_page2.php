<!DOCTYPE html>
<html lang="en">


<head>
	<meta charset=UTF-8/>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>

<?php
    // $answer6 = $answer7 = $answer8 = $answer9 = $answer10 = '';
    // session_start();
    // $_SESSION['question-6-answers'] = $_POST['question-6-answers'];
    // $_SESSION['question-7-answers'] = $_POST['question-7-answers'];
    // $_SESSION['question-8-answers'] = $_POST['question-8-answers'];
    // $_SESSION['question-9-answers'] = $_POST['question-9-answers'];
    // $_SESSION['question-10-answers'] = $_POST['question-10-answers'];
?>
 
<body>
 
	<div id="page-wrap">
 
		<h1>Simple Quiz Built On PHP</h1>
		
		<form action="day09_result.php" method="post" id="quiz">
            <ol>

                <li>
                    <h3>Which of the following is true about php variables?</h3>
                    
                    <div>
                        <input type="radio" name="question-6-answers" id="question-6-answers-A" value="A" />
                        <input type="hidden" name="question-1-answers" value="<?php  echo $_POST['question-1-answers']; ?>">
                        <label for="question-6-answers-A">A) All variables in PHP are denoted with a leading dollar sign ($). </label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-6-answers" id="question-6-answers-B" value="B" />
                        <label for="question-6-answers-B">B) The value of a variable is the value of its most recent assignment.</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-6-answers" id="question-6-answers-C" value="C" />
                        <label for="question-6-answers-C">C) Variables are assigned with the = operator, with the variable 
                            on the left-hand side and the expression to be evaluated on the right.</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-6-answers" id="question-6-answers-D" value="D" />
                        <label for="question-6-answers-D">D) All of the above.</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>Which of the following type of variables are special variables that hold 
                        references to resources external to PHP (such as database connections)?</h3>
                    
                    <div>
                        <input type="radio" name="question-7-answers" id="question-7-answers-A" value="A" />
                        <input type="hidden" name="question-2-answers" value="<?php  echo $_POST['question-2-answers']; ?>">
                        <label for="question-7-answers-A">A) Strings</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-7-answers" id="question-7-answers-B" value="B" />
                        <label for="question-7-answers-B">B) Arrays</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-7-answers" id="question-7-answers-C" value="C" />
                        <label for="question-7-answers-C">C) Objects</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-7-answers" id="question-7-answers-D" value="D" />
                        <label for="question-7-answers-D">D) Resources</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>Which of the following is correct about NULL?</h3>
                    
                    <div>
                        <input type="radio" name="question-8-answers" id="question-8-answers-A" value="A" />
                        <input type="hidden" name="question-3-answers" value="<?php  echo $_POST['question-3-answers']; ?>">
                        <label for="question-8-answers-A">A) A variable that has been assigned NULL evaluates to FALSE in a Boolean context.</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-8-answers" id="question-8-answers-B" value="B" />
                        <label for="question-8-answers-B">B) A variable that has been assigned NULL returns FALSE when tested with IsSet() function.</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-8-answers" id="question-8-answers-C" value="C" />
                        <label for="question-8-answers-C">C) Both of the above.</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-8-answers" id="question-8-answers-D" value="D" />
                        <label for="question-8-answers-D">D) None of the above.</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>Which of the following array represents an array with a numeric index?</h3>
                    
                    <div>
                        <input type="radio" name="question-9-answers" id="question-9-answers-A" value="A" />
                        <input type="hidden" name="question-4-answers" value="<?php  echo $_POST['question-4-answers']; ?>">
                        <label for="question-9-answers-A">A) Numeric Array</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-9-answers" id="question-9-answers-B" value="B" />
                        <label for="question-9-answers-B">B) Associative Array</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-9-answers" id="question-9-answers-C" value="C" />
                        <label for="question-9-answers-C">C) Multidimentional Array</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-9-answers" id="question-9-answers-D" value="D" />
                        <label for="question-9-answers-D">D) None of the above.</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>Which of the following variable is used to generate random numbers using PHP?</h3>
                    
                    <div>
                        <input type="radio" name="question-10-answers" id="question-10-answers-A" value="A" />
                        <input type="hidden" name="question-5-answers" value="<?php  echo $_POST['question-5-answers']; ?>">
                        <label for="question-10-answers-A">A) srand()</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-10-answers" id="question-10-answers-B" value="B" />
                        <label for="question-10-answers-B">B) rand()</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-10-answers" id="question-10-answers-C" value="C" />
                        <label for="question-10-answers-C">C) random()</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-10-answers" id="question-10-answers-D" value="D" />
                        <label for="question-10-answers-D">D) None of the above.</label>
                    </div>
                
                </li>
            
            </ol>
            
            <button type="submit" value="Submit" class="submitbtn">Submit </button>
		
		</form>
	
	</div>
    
 
</body>
 
</html>